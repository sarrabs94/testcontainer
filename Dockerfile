FROM python:2.7
ADD testcontainer.py /
RUN pip install flask
EXPOSE 5000
CMD ["python", "./testcontainer.py"]
